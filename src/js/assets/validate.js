export default class Validate {
  static #isEmpty = (input) => {
    return input.value.toString().trim() === "";
  };

  static #isEmailValid = (email) => {
    const re =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  };

  static checkRequired = (input, success, error, errorMessage) => {
    if (Validate.#isEmpty(input)) {
      error(input, errorMessage);
    } else {
      success(input);
    }
  };

  static checkLength = (
    input,
    min,
    max,
    success,
    error,
    messageMin,
    messgaeMax
  ) => {
    if (input.value.length < min) {
      error(input, messageMin);
    } else if (input.value.length > max) {
      error(input, messgaeMax);
    } else {
      success(input);
    }
  };

  static checkEmail = (input, success, error, errorMessage) => {
    if (!Validate.#isEmailValid(input.value)) {
      error(input, errorMessage);
    } else {
      success(input);
    }
  };
}
