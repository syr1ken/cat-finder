Number.prototype.format = function (n, x) {
  var re = "\\d(?=(\\d{" + (x || 3) + "})+" + (n > 0 ? "\\." : "$") + ")";
  return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, "g"), "$& ");
};

const showError = (error, className, message) => {
  error.innerHTML = message;
  error.classList.add(className);
};

export const elements = {
  rootElement: document.documentElement,
  heroTitle: document.getElementById("hero__title"),
  btnBars: document.getElementById("btn-bars"),
  navMenu: document.getElementById("nav__menu"),
  ctaCheckbox: document.getElementById("cta__checkbox"),
  ctaMarkbox: document.getElementById("cta__markbox"),
  btnTopScroll: document.getElementById("btn-topscroll"),
  btnSortByPrice: document.getElementById("sort__price"),
  btnSortByAge: document.getElementById("sort__age"),
  cardsContainer: document.getElementById("cards-container"),
  btnViewMore: document.getElementById("viewmore"),
  veiwMoreContainer: document.getElementById("viewmore__container"),
  cardsInfo: document.getElementById("cards__info"),
  ctaFormEmail: document.getElementById("cta__form-email"),
  ctaFormButton: document.getElementById("cta__form-btn"),
  inputErrorElement: document.getElementById("cta__message")
};

export const elementStrings = {
  active: "active",
  checked: "checked",
  likeImagePath: "./img/like.svg",
  likeActiveImagePath: "./img/like-active.svg",
  dataSortAttr: "data-sort",
  sortHighToLow: "htl",
  sortLowToHigh: "lth",
  catPirce: "catPrice",
  cardPriceNumber: "card__price-number",
  catAge: "catAge",
  footerMargin: "footer--margin",
  cardLikeClass: "card__like",
  cardLikeMessage: "card__like-message",
  cardLikeMessageAdded: "Добавлено в избранное",
  cardLikeMessageRemoved: "Удалено из избранного",
  likeNotificationShow: "show",
  card: "card",
  inputSuccess: "cta__form-email--success",
  inputError: "cta__form-email--error",
  id: "id",
  img: "img",
  src: "src"
};

export const error = (input, errorMessage) => {
  showError(elements.inputErrorElement, elementStrings.active, errorMessage);
  input.classList.add(elementStrings.inputError);
};

export const success = (input) => {
  input.classList.add(elementStrings.inputSuccess);
};

export const getFieldName = (element) => {
  if (element.name === "") {
    return "Input";
  }
  return element.name.charAt(0).toUpperCase() + element.name.slice(1);
};

export const removeState = (element, className) => {
  element.classList.remove(className);
};
