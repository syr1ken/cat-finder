import { elements, elementStrings } from "../views/base";

const sortBtnInit = (btn) => {
  btn.setAttribute(elementStrings.dataSortAttr, elementStrings.sortHighToLow);
  btn.classList.add(elementStrings.active);
};

const createCardTemplate = (card, likedCards) => {
  const state = {
    disabled: "",
    text: "Купить",
    discount: "",
    active: "",
    likeImg: elementStrings.likeImagePath
  };

  if (card.isDiscount) {
    state.discount = `<div class="card__discount">-${card.discount}%</div>`;
  }

  if (card.isSold) {
    state.disabled = "disabled";
    state.text = "Продан";
  }

  const isActive = likedCards.filter((cardItem) => {
    return cardItem.id === card.id;
  });

  if (isActive.length != 0) {
    state.active = elementStrings.active;
    state.likeImg = elementStrings.likeActiveImagePath;
  }

  const html = `
  <div class="card__wrap" data-cardid="${card.id}">
    <article class="card">
      <div id="card__like-message" class="card__like-message"></div>
      ${state.discount}
      <div class="card__like-container">
          <button class="card__like ${state.active}"><img src="${
    state.likeImg
  }" /></button>
      </div>
      <div
        class="card__img"
        style="background-image: url(${card.catImg})"
      ></div>
      <h2 class="card__title">${card.catTitle}</h2>
      <div class="card__group">
        <div class="card__color">${card.catColor}</div>
        <div class="card__age"><span>${card.catAge} мес.</span>Возраст</div>
        <div class="card__paws"><span>${card.catPaws}</span>Кол-во лап</div>
      </div>
      <h2 class="card__price">${card.catPrice.format()} руб.</h2>
      <button class="card__btn" ${state.disabled}>${state.text}</button>
    </article>
  </div>`;

  return html;
};

const render = (cards, likedCards) => {
  if (!cards || cards.length === 0) return;
  let html = ``;

  // Loop through whole array, create and append cards templates to html
  cards.forEach((card) => {
    const cardTemplate = createCardTemplate(card, likedCards);
    html += cardTemplate;
  });

  elements.cardsContainer.innerHTML = html;
};

const showNotification = (card, text) => {
  // Find the card message element
  const cardLikeMessage = card.querySelector(
    `.${elementStrings.cardLikeMessage}`
  );

  // Add text to the card message element
  cardLikeMessage.innerHTML = text;

  // Add class to a message element
  cardLikeMessage.classList.add(elementStrings.likeNotificationShow);

  // Add timeout to a message element to remove show class agter 2 seconds
  setTimeout(
    () => cardLikeMessage.classList.remove(elementStrings.likeNotificationShow),
    2000,
    cardLikeMessage
  );
};

const disableViewMore = (filteredCards, cards) => {
  if (filteredCards.length >= cards.length) {
    elements.btnViewMore.classList.remove(elementStrings.active);
    elements.cardsInfo.classList.add(elementStrings.active);
  }
};

export { render, sortBtnInit, disableViewMore, showNotification };
