// Import Custom Styles
import "../scss/styles.scss";

// Images
const images = {
  logo: require("../img/logo.svg"),
  btnBars: require("../img/bars.svg"),
  catOne: require("../img/cat_1.png"),
  catTwo: require("../img/cat_2.png"),
  catThree: require("../img/cat_3.png"),
  cardLike: require("../img/like.svg"),
  cardLike: require("../img/like-active.svg")
};

// Views
import { elements, elementStrings } from "./views/base";
import * as cardView from "./views/cardView";

// Controllers
import * as topScrollHandler from "./controllers/topScrollHandler";
import * as checkboxHandler from "./controllers/checkboxHandler";
import * as menuHandler from "./controllers/menuHandler";
import * as cardHandler from "./controllers/cardHandler";
import * as ctaHandler from "./controllers/ctaFormHandler";

// Models
import * as Card from "./models/Card";

// State Init
const state = {
  cards: [],
  filteredCards: [],
  likedCards: [],
  maxDisplayLimit: Card.CARDS_TO_SHOW
};

// Initiate App

const initiateApp = async () => {
  // Initiate sort btns
  cardView.sortBtnInit(elements.btnSortByPrice);
  cardView.sortBtnInit(elements.btnSortByAge);

  // Fetch card data
  state.cards = await Card.fetchCards();

  // Fill hero title
  elements.heroTitle.innerHTML = `Найдено ${state.cards.length} котов`;

  // Get amount of cards that has to be rendered
  state.filteredCards = state.cards.slice(0, state.maxDisplayLimit);

  // Render cards
  cardView.render(state.filteredCards, state.likedCards);

  cardHandler.addLikeButtonClickEvent(state);

  // Check if render View More Button
  cardView.disableViewMore(state.filteredCards, state.cards);
};

// Event Listeners
// On load
document.addEventListener("DOMContentLoaded", initiateApp);

// Scroll
window.onscroll = () => topScrollHandler.scrollFunction();

// elements.btnTopScroll.addEventListener("click", topScrollHandler.scrollToTop);

// Header Menu
elements.btnBars.addEventListener("click", menuHandler.toggleMenu);

// Checkbox
elements.ctaMarkbox.addEventListener("click", checkboxHandler.toggleCheckbox);

// Sort By Price Button
elements.btnSortByPrice.addEventListener("click", function () {
  // Sort state.filteredCards Array
  state.filteredCards = cardHandler.sortByPrice(state.filteredCards);

  cardView.render(state.filteredCards, state.likedCards);
  cardHandler.addLikeButtonClickEvent(state);
});

// Sort By Age Button
elements.btnSortByAge.addEventListener("click", function () {
  // Sort state.filteredCards Array
  state.filteredCards = cardHandler.sortByAge(state.filteredCards);

  cardView.render(state.filteredCards, state.likedCards);
  cardHandler.addLikeButtonClickEvent(state);
});

// View More Cards Button
elements.btnViewMore.addEventListener("click", function () {
  cardHandler.viewMoreCards(state);
  cardHandler.addLikeButtonClickEvent(state);

  // Check if render View More Button
  cardView.disableViewMore(state.filteredCards, state.cards);
});

// CTA form submit
elements.ctaFormButton.addEventListener("click", ctaHandler.submit);
