import * as Card from "../models/Card";
import { elements, elementStrings } from "../views/base";
import * as cardView from "../views/cardView";

const sort = (cards, field, btn) => {
  // Sort High To Low
  if (btn.dataset.sort === elementStrings.sortHighToLow) {
    cards.sort((a, b) => {
      return a[field] - b[field];
    });

    // Set data-sort attribute to lowest to highest sort on next click
    btn.setAttribute(elementStrings.dataSortAttr, elementStrings.sortLowToHigh);

    // Remove Active Class
    btn.classList.remove(elementStrings.active);
    return cards;
  }

  // Sort Low To High
  if (btn.dataset.sort === elementStrings.sortLowToHigh) {
    cards.sort((a, b) => {
      return b[field] - a[field];
    });

    // Set data-sort attribute to highest to lowest sort on next click
    btn.setAttribute(elementStrings.dataSortAttr, elementStrings.sortHighToLow);

    // Add Active Class
    btn.classList.add(elementStrings.active);
    return cards;
  }
};

const sortByPrice = (cards) => {
  const sortedCards = sort(
    cards,
    elementStrings.catPirce,
    elements.btnSortByPrice
  );

  return sortedCards;
};

const sortByAge = (cards) => {
  const sortedCards = sort(cards, elementStrings.catAge, elements.btnSortByAge);
  return sortedCards;
};

const viewMoreCards = (state) => {
  state.maxDisplayLimit += Card.CARDS_TO_SHOW;

  // Get amount of cards that has to be rendered
  state.filteredCards = state.cards.slice(0, state.maxDisplayLimit);

  // Render cards
  cardView.render(state.filteredCards, state.likedCards);
};

const addLikeButtonClickEvent = (state) => {
  // Add Event listeners for each like button
  const cardsContainerArray = Array.prototype.slice.call(
    elements.cardsContainer.children
  );

  cardsContainerArray.forEach((card) => {
    const cardLikeBtn = card.querySelector(`.${elementStrings.cardLikeClass}`);
    cardLikeBtn.addEventListener("click", (e) => {
      // Find Card
      const card = e.target.closest(`.${elementStrings.card}`);

      /// Find Card Button
      const cardLikeButton = card.querySelector(
        `.${elementStrings.cardLikeClass}`
      );

      // Find Card Img
      const cardLikeImg = cardLikeButton.querySelector(elementStrings.img);

      // Find Card ID
      const id = card.parentNode.dataset.cardid;

      // Find Card in filteredCards
      const dataCardArray = state.filteredCards.filter((card) => {
        return card.id === id;
      });

      if (!state.likedCards.includes(dataCardArray[0])) {
        state.likedCards.push(dataCardArray[0]);

        cardLikeButton.classList.add(elementStrings.active);
        cardLikeImg.setAttribute(
          elementStrings.src,
          elementStrings.likeActiveImagePath
        );

        cardView.showNotification(card, elementStrings.cardLikeMessageAdded);
      } else {
        state.likedCards = state.likedCards.filter((cardItem) => {
          return cardItem !== dataCardArray[0];
        });

        cardLikeButton.classList.remove(elementStrings.active);
        cardLikeImg.setAttribute(
          elementStrings.src,
          elementStrings.likeImagePath
        );

        cardView.showNotification(card, elementStrings.cardLikeMessageRemoved);
      }
    });
  });
};

export { viewMoreCards, sortByPrice, sortByAge, addLikeButtonClickEvent };
