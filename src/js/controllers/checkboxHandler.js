import { elements, elementStrings } from "../views/base";

const toggleCheckbox = (e) => {
  e.preventDefault();

  // Added IE 11 supprot - without toggleAttribute()
  if (elements.ctaCheckbox.hasAttribute(elementStrings.checked)) {
    elements.ctaCheckbox.removeAttribute(elementStrings.checked);
    return;
  }

  elements.ctaCheckbox.setAttribute(elementStrings.checked, "");
};

export { toggleCheckbox };
