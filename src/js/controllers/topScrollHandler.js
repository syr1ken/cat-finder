const SmoothScroll = require("smooth-scroll");

import { elements, elementStrings } from "../views/base";

// const scrollToTop = (e) => {
//   e.preventDefault();

//   // Doesn't work on IE 11
//   // elements.rootElement.scrollTo({
//   //   top: 0,
//   //   behavior: "smooth"
//   // });

//   // Without smooth scroll on IE 11
//   // document.body.scrollTop = 0;
//   // document.documentElement.scrollTop = 0;
// };

const scroll = new SmoothScroll("#btn-topscroll", {
  speed: 100
});

const scrollFunction = () => {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    elements.btnTopScroll.classList.add(elementStrings.active);
  } else {
    elements.btnTopScroll.classList.remove(elementStrings.active);
  }
};

export { scrollFunction };
