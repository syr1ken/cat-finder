import { elements, elementStrings } from "../views/base";

const toggleMenu = (e) => {
  e.preventDefault();
  elements.navMenu.classList.toggle(elementStrings.active);
};

export { toggleMenu };
