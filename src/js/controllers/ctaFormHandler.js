import {
  elements,
  elementStrings,
  success,
  error,
  getFieldName,
  removeState
} from "../views/base";
import Validate from "../assets/validate";

const submit = (e) => {
  e.preventDefault();

  removeState(elements.ctaFormEmail, elementStrings.inputError);
  removeState(elements.ctaFormEmail, elementStrings.inputSuccess);
  removeState(elements.inputErrorElement, elementStrings.active);

  const min = 6;
  const max = 30;

  Validate.checkRequired(
    elements.ctaFormEmail,
    success,
    error,
    `${getFieldName(elements.ctaFormEmail)} is required`
  );

  Validate.checkLength(
    elements.ctaFormEmail,
    min,
    max,
    success,
    error,
    `Must be at least ${min} characters`,
    `Must be less than ${max} characters`
  );

  Validate.checkEmail(
    elements.ctaFormEmail,
    success,
    error,
    `Enter valid email`
  );
};

export { submit };
