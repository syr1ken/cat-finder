const CARDS_TO_SHOW = 6;

const catsData = [
  {
    id: "1",
    catTitle: "Кот полосатый",
    catImg: "./img/cat_1.png",
    catColor: "Коричневый",
    catAge: 3,
    catPaws: 4,
    catPrice: 27000,
    isDiscount: true,
    discount: 40,
    isSold: false
  },
  {
    id: "2",
    catTitle: "Кот красивый",
    catImg: "./img/cat_2.png",
    catColor: "Коричневый",
    catAge: 6,
    catPaws: 4,
    catPrice: 40000,
    isDiscount: false,
    isSold: true
  },
  {
    id: "3",
    catTitle: "Кот хитрый",
    catImg: "./img/cat_3.png",
    catColor: "Коричневый",
    catAge: 5,
    catPaws: 4,
    catPrice: 20000,
    isDiscount: false,
    isSold: false
  },
  {
    id: "4",
    catTitle: "Кот полосатый",
    catImg: "./img/cat_1.png",
    catColor: "Коричневый",
    catAge: 3,
    catPaws: 4,
    catPrice: 25000,
    isDiscount: true,
    discount: 50,
    isSold: false
  },
  {
    id: "5",
    catTitle: "Кот красивый",
    catImg: "./img/cat_2.png",
    catColor: "Коричневый",
    catAge: 1,
    catPaws: 4,
    catPrice: 30000,
    isDiscount: false,
    isSold: false
  },
  {
    id: "6",
    catTitle: "Кот хитрый",
    catImg: "./img/cat_3.png",
    catColor: "Коричневый",
    catAge: 2,
    catPaws: 4,
    catPrice: 10000,
    isDiscount: false,
    isSold: true
  },
  {
    id: "7",
    catTitle: "Кот полосатый",
    catImg: "./img/cat_1.png",
    catColor: "Коричневый",
    catAge: 3,
    catPaws: 4,
    catPrice: 5000,
    isDiscount: true,
    discount: 90,
    isSold: false
  },
  {
    id: "8",
    catTitle: "Кот красивый",
    catImg: "./img/cat_2.png",
    catColor: "Коричневый",
    catAge: 6,
    catPaws: 4,
    catPrice: 33000,
    isDiscount: false,
    isSold: true
  },
  {
    id: "9",
    catTitle: "Кот хитрый",
    catImg: "./img/cat_3.png",
    catColor: "Коричневый",
    catAge: 2,
    catPaws: 4,
    catPrice: 16000,
    isDiscount: false,
    isSold: false
  },
  {
    id: "10",
    catTitle: "Кот полосатый",
    catImg: "./img/cat_1.png",
    catColor: "Коричневый",
    catAge: 3,
    catPaws: 4,
    catPrice: 2000,
    isDiscount: true,
    discount: 85,
    isSold: false
  },
  {
    id: "11",
    catTitle: "Кот красивый",
    catImg: "./img/cat_2.png",
    catColor: "Коричневый",
    catAge: 6,
    catPaws: 4,
    catPrice: 19000,
    isDiscount: false,
    isSold: true
  },
  {
    id: "12",
    catTitle: "Кот хитрый",
    catImg: "./img/cat_3.png",
    catColor: "Коричневый",
    catAge: 2,
    catPaws: 4,
    catPrice: 21000,
    isDiscount: true,
    discount: 15,
    isSold: false
  },
  {
    id: "13",
    catTitle: "Кот красивый",
    catImg: "./img/cat_2.png",
    catColor: "Коричневый",
    catAge: 6,
    catPaws: 4,
    catPrice: 24000,
    isDiscount: false,
    isSold: true
  },
  {
    id: "14",
    catTitle: "Кот хитрый",
    catImg: "./img/cat_3.png",
    catColor: "Коричневый",
    catAge: 2,
    catPaws: 4,
    catPrice: 16000,
    isDiscount: false,
    isSold: false
  }
];

const fetchCards = () => {
  return catsData;
};

export { CARDS_TO_SHOW, fetchCards };
